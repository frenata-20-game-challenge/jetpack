extends CharacterBody2D

@export var speed: float

func _physics_process(delta):
	var collision = move_and_collide(Vector2.LEFT * speed * delta)
	if collision:
		var collider = collision.get_collider()
		if collider.has_method("take_damage"):
			collider.take_damage(20)
			_die()

func take_damage(_amount):
	_die()
	
func _die():
	%Explosion.play()
	$CollisionShape2D.disabled = true
	visible = false
	await %Explosion.finished
	queue_free()
