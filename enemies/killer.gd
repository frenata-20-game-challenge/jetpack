extends Area2D


func _on_body_entered(body):
	if body.is_in_group(get_parent().group_name):
		body.queue_free()
