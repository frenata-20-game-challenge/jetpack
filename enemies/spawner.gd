extends Node2D

@export var group_name: String
@export var speed: float
@export var obstacle_scene: PackedScene
@export_range (0.1, 10.0) var min_spawn: float
@export_range (0.1, 10.0) var max_spawn: float
@export var spawn_height: Callable
@export var spawn_rate: Callable

signal spawn(obstacle)

func _spawn():
	
	var obstacle = obstacle_scene.instantiate()
	obstacle.speed = speed
	obstacle.position.y = %Spawner.position.y + spawn_height.call()
	obstacle.position.x = %Spawner.position.x

	obstacle.add_to_group("obstacles")
	obstacle.add_to_group(group_name)
	add_child(obstacle)
	spawn.emit(obstacle)

	%Timer.start(spawn_rate.call())
