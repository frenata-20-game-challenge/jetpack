# Jetpack Joyride

## Things I'm Happy With

 * Sticking with system composability. At one point I had an "enemy manager" scene that was sending spawned enemies through a signal to its parent for add_child... and then I realized it was totally unnecessary, that the entire "enemy manager" subtree _didn't_ need to communicate upwards! This paid huge dividends when I changed the ceiling from "do damage on contact" to a different kind of spawned enemy.
 * Music! Sound effects! Hand crafted animations!
 * I was very surprised that saving state between sessions was easy, without having to write any target platform specific logic.
 * Lambda functions with internally managed state (via `var state = {'foo': 0}`)
 
## Things I'd Like To Improve

 * I still feel like there's a better patten available to express 'floor movement'. What I have *works* (two long floors that match up at the ends and get rotated around).
 * I'd like to add a background scene that rotates too, but at a slightly slower speed to give the illusion of dimensionality.
 * Add a list of high scores with player initials, arcade style.
 * Figure out why this fails to build properly in CI. :( Possibly this issue though: https://github.com/godotengine/godot/issues/70910
 
## Deploy

https://tourmaline-trifle-efc59f.netlify.app/