extends Node2D

@export var points_per_second = 10
@export var scroll_speed = 300

signal score(points)
signal player_death

func _make_batch_spawn():
	var state = {"time_between_batch": 4, "number_in_batch": 6}
	var _spawn = func():
		if state.number_in_batch > 0:
			state.number_in_batch -= 1
			return 0.1
		else:
			var wait = state.time_between_batch
			state.time_between_batch = randf_range(0.6,3)
			state.number_in_batch = randi_range(3,9)
			return wait
	return _spawn
	
func _make_quickening_spawn():
	var state = {"max_spawn": 2.0}
	var _spawn = func():
		if state.max_spawn > 1.0:
			state.max_spawn -= 0.005
		return randf_range(0.2, state.max_spawn)
	return _spawn

func _ready():
	%Floors.speed = 300
	
	%Bombs.speed = 300
	%Bombs.spawn_height = func(): return randi_range(-250, 290)
	%Bombs.spawn_rate = _make_quickening_spawn()
	
	%Spikes.speed = 300
	%Spikes.spawn_height = func(): return 0
	%Spikes.spawn_rate = _make_batch_spawn()

func _on_score(points = points_per_second):
	score.emit(points)
