extends CharacterBody2D

func _physics_process(delta):
	var collision = move_and_collide(Vector2.LEFT * get_parent().speed * delta)

	if collision:
		if collision.get_collider().is_in_group("rotator"):
			set_position(Vector2(position.x + 2175*2 - 50, -16))
