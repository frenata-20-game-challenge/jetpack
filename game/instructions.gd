extends Label


func _ready():
	await get_tree().create_timer(5).timeout
	text = "Shoot enemies to score extra points."
	await get_tree().create_timer(5).timeout
	text = "Watch out for ceiling spikes!"
	await get_tree().create_timer(5).timeout
	text = "Stay alive as long as possible."
	await get_tree().create_timer(5).timeout
	text = ""

func _on_game_over():
	text = "Press 'r' to try for a higher score."
	await get_tree().create_timer(5).timeout
	text = ""
