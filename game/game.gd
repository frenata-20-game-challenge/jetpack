extends Node2D

var high_score = 0
var level
var player
var level_scene = preload("res://world/level.tscn")
var player_scene = preload("res://player/player.tscn")

func _ready():
	%Score.set_text("0")
	%"High Score".set_text(str(load_scores()))
	level = level_scene.instantiate()
	level.score.connect(_on_score)
	add_child(level)
	
	player = player_scene.instantiate()
	player.death.connect(_on_player_death)
	player.death.connect(%Instructions._on_game_over)
	player.position = Vector2(300,400)
	add_child(player)

func _on_player_death():
	save_scores()
	level.queue_free()
	player.queue_free()
	level = null
	player = null


func _on_score(points):
	%Score.set_text(str(int(%Score.text) + points) )

func _input(event):
	if event.is_action_pressed("restart"):
		if level == null:
			_ready()
			
			
func save_scores():
	var current_score = int(%Score.text)
	var highscore = int(%"High Score".text)
	if current_score > highscore:
		%"High Score".set_text(str(current_score))

		var scores = FileAccess.open("user://scores.json", FileAccess.WRITE)
		scores.store_line(JSON.stringify(current_score))

			
func load_scores() -> int:
	var scores = FileAccess.open("user://scores.json", FileAccess.READ)
	if scores == null:
		return 0
	return int(JSON.parse_string(scores.get_line()))


func _on_music_finished():
	%Music.play()
