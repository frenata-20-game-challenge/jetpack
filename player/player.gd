extends CharacterBody2D

@export var speed = 300.0
@export var jump = -250.0
@export var rate_of_fire = 4

signal death

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var bullet_scene = preload("res://player/bullet.tscn")

func shoot_bullet(vector):
	var bullet = bullet_scene.instantiate()
	bullet.velocity = vector
	bullet.position = Vector2(position.x - 14, position.y + 24)
	bullet.hit_obstacle.connect(get_parent()._on_score)
	get_parent().add_child(bullet)
	
var reload = 0

func _physics_process(delta):
	if reload > 0:
		reload = (reload + 1) % rate_of_fire
		
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle Jump.
	if Input.is_action_pressed("ui_accept"):
		velocity.y = jump
		
		if reload == 0:
			%MachineGun.play()
			shoot_bullet(Vector2(0, 5))
			shoot_bullet(Vector2(1,4))
			shoot_bullet(Vector2(-1,4))
			reload = (reload +1) % rate_of_fire
		
	var collision = move_and_collide(velocity * delta)


func take_damage(amount):
	%Health.set_value(%Health.value - amount)
	if not %Pain.playing:
		%Pain.play()
	

func _process(delta):
	if %Health.value == 0:
		death.emit()
