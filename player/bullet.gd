extends CharacterBody2D

signal hit_obstacle(points)

const SPEED = 250.0

func _physics_process(delta):
	
	var collision = move_and_collide(velocity * SPEED * delta)
	if collision:
		
		var collider = collision.get_collider()
		var is_obstacle = collider.is_in_group("obstacles")
		if is_obstacle:
			collider.take_damage(4)

			hit_obstacle.emit(100)
		queue_free()
